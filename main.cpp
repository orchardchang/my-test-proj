#include <iostream>
#include <unordered_map>
#include <vector>
#include <stack>

using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode (int x) : val(x), next(nullptr) {}
};

ListNode* revNode(ListNode* head) {
    ListNode *prev = nullptr, *next;
    ListNode *curr = head;
    while (curr) {
        next = curr->next;
        curr->next = prev;
        prev = curr;
        curr = next;
    }
    return prev;
}
bool isPalindrome(ListNode* head) {
    if (head->next==nullptr) {
        return true;
    }
    ListNode *slow = head;
    ListNode *fast = head->next;
    if (fast->next == nullptr) {
        return slow->val == fast->val;
    }
    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
    }
    ListNode* recovernode = revNode(slow->next);
    ListNode* curnode = head;
    ListNode* revnode = recovernode;
    while (revnode) {
        if (curnode->val != revnode->val) {
            return false;
        }
        curnode = curnode->next;
        revnode = revnode->next;
    }
    slow->next = revNode(recovernode);
    return true;
}

class Solution {
public:
    vector<string> letterCombinations(string digits) {
        vector<string> combinations;
        if (digits.empty()) {
            return combinations;
        }
        unordered_map<char, string> phoneMap{
                {'2', "abc"},
                {'3', "def"},
                {'4', "ghi"},
                {'5', "jkl"},
                {'6', "mno"},
                {'7', "pqrs"},
                {'8', "tuv"},
                {'9', "wxyz"}
        };
        string combination;
        backtrack(combinations, phoneMap, digits, 0, combination);
        return combinations;
    }

    void backtrack(vector<string>& combinations, const unordered_map<char, string>& phoneMap, const string& digits, int index, string& combination) {
        if (index == digits.length()) {
            combinations.push_back(combination);
        } else {
            char digit = digits[index];
            const string& letters = phoneMap.at(digit);
            for (const char& letter: letters) {
                combination.push_back(letter);
                backtrack(combinations, phoneMap, digits, index + 1, combination);
                combination.pop_back();
            }
        }
    }
};

int main() {
    ListNode *head = new ListNode(1);
    head->next = new ListNode(2);
    head->next->next = new ListNode(3);
    head->next->next->next = new ListNode(4);
    head->next->next->next->next = new ListNode(5);
    cout << isPalindrome(head) << endl;
    return 0;
}
